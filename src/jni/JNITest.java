package jni;

public class JNITest {
    public JNITest(String name) {
        System.loadLibrary(name);
    }

    public native int getMove(int [] fields);
}
