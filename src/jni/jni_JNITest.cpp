#include "jni_JNITest.h"
#include <time.h>

using namespace std;

JNIEXPORT jint JNICALL Java_jni_JNITest_getMove (JNIEnv * env, jobject object, jintArray array) {
    srand (time(NULL));

    jsize len = env->GetArrayLength(array);
    jint * fields = env->GetIntArrayElements(array, NULL);

    int emptyFieldsCount = 0;

    for(int i = 0; i < len; i++) {
        if(fields[i] == 0) {
            emptyFieldsCount++;
        }
    }

    int * emptyFields = new int[emptyFieldsCount];

    int j = 0;
    for(int i = 0; i < len; i++) {
        if(fields[i] == 0) {
            emptyFields[j] = i;
            j++;
        }
    }

    int index = rand() % j;
    int fieldIndex = emptyFields[index];

    delete [] emptyFields;

    return fieldIndex;
}