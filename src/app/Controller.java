package app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import jni.JNITest;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Controller {
    ArrayList<Button> buttons = new ArrayList<>();

    @FXML Button btn1;
    @FXML Button btn2;
    @FXML Button btn3;
    @FXML Button btn4;
    @FXML Button btn5;
    @FXML Button btn6;
    @FXML Button btn7;
    @FXML Button btn8;
    @FXML Button btn9;
    @FXML Button btn10;
    @FXML Button btn11;
    @FXML Button btn12;
    @FXML Button btn13;
    @FXML Button btn14;
    @FXML Button btn15;
    @FXML Button btn16;
    @FXML Button clearScriptButton;

    @FXML Text winnerText;
    @FXML Text playerScore;
    @FXML Text computerScore;
    @FXML Text selectedFileText;

    private int move = 0;
    private int playerScoreCount = 0,
        computerScoreCount = 0;

    private int [] fields = new int[16];

    private Invocable invocable = null;
    private JNITest jnitest = null;

    public void buttonClick(ActionEvent actionEvent) throws ScriptException, NoSuchMethodException {
        Button btn = (Button) actionEvent.getSource() ;
        String data = (String) btn.getUserData();
        int value = Integer.parseInt(data);

        if(!winnerText.getText().equals("")) {
            winnerText.setText("");
        }

        if(!btn.getText().equals("")) {
            return;
        }

        if(move == 0) {
            btn.setText("X");
            fields[value - 1] = 1;
        } else {
            btn.setText("O");
            fields[value - 1] = 2;
        }

        move++;

        if(move == 2) {
            move = 0;
        }

        if(won(checkWin())) {
            return;
        }

        if(move == 1) {
            computerMove();
        }
    }

    public void buttonNewGame(ActionEvent actionEvent) {
        newGame();
    }

    public void selectFile(ActionEvent actionEvent) throws ScriptException, FileNotFoundException, NoSuchMethodException {
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(null);

        if(selectedFile != null) {
            disableButtons(true);

            if(getFileExtension(selectedFile).equals(".js")) {
                if(jnitest != null) {
                    jnitest = null;
                }

                selectedFileText.setText(selectedFile.getName());

                ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
                engine.eval(new FileReader(selectedFile.getAbsolutePath()));
//                engine.eval(new FileReader("C:\Users\Cezary\Desktop\PWr\Java\java-lab-9\ai\minimax.js"));

                invocable = (Invocable) engine;

                computerMove();

                clearScriptButton.setDisable(false);
            } else if (getFileExtension(selectedFile).equals(".dylib")) {
                if(invocable != null) {
                    invocable = null;
                }


                String name = selectedFile.getName().replace(getFileExtension(selectedFile), "").replace("lib", "");

                jnitest = new JNITest(name);

                clearScriptButton.setDisable(false);
            }

            disableButtons(false);
        }
    }

    public void computerMove() throws ScriptException, NoSuchMethodException {
        disableButtons(true);

        if(move == 1 && invocable != null) {
            Double result = (Double) invocable.invokeFunction("getMove", fields);

            Integer index = result.intValue();
            fields[index] = 2;
            buttons.get(index).setText("O");
            move = 0;

            if(won(checkWin())) {
                return;
            }
        } else if(move == 1 && jnitest != null) {
            Integer index = jnitest.getMove(fields);

            fields[index] = 2;
            buttons.get(index).setText("O");
            move = 0;

            if(won(checkWin())) {
                return;
            }
        }

        disableButtons(false);
    }

    public void clearScript(ActionEvent event) {
        invocable = null;
        jnitest = null;
        selectedFileText.setText("-");
        clearScriptButton.setDisable(true);
    }

    private int checkWin() {
        int counter = 0,
                type = 0;

        // poziom
        for(int i = 0; i < 16; i++) {
            if(fields[i] == 0 || i == 4 || i == 8 || i == 12) {
                type = 0;
                counter = 0;
            }

            if(fields[i] > 0) {
                if(type == 0) {
                    type = fields[i];
                    counter = 1;
                } else if(type == fields[i]) {
                    counter++;
                } else if(type != fields[i]) {
                    type = fields[i];
                    counter = 1;
                }
            }

            if(counter == 3) {
                return type;
            }
        }

        counter = 0;
        type = 0;

        // pion
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                int index = (i + (j * 4));

                if(fields[index] > 0) {
                    if(type == 0) {
                        type = fields[index];
                        counter = 1;
                    } else if(type == fields[index]) {
                        counter++;
                    } else if(type != fields[index]) {
                        type = fields[index];
                        counter = 1;
                    }
                } else {
                    type = 0;
                    counter = 0;
                }

                if(counter == 3) {
                    return type;
                }
            }

            counter = 0;
            type = 0;
        }

        counter = 0;
        type = 0;

        // skosy

        int [][] skosy = {
                {1, 6, 11},
                {0, 5, 10, 15},
                {4, 9, 14},
                {2, 5, 8},
                {3, 6, 9, 12},
                {7, 10, 13},
        };

        for(int i = 0; i < skosy.length; i++) {
            for(int j = 0; j < skosy[i].length; j++) {
                int index = skosy[i][j];

                if(fields[index] > 0) {
                    if(type == 0) {
                        type = fields[index];
                        counter = 1;
                    } else if(type == fields[index]) {
                        counter++;
                    } else if(type != fields[index]) {
                        type = fields[index];
                        counter = 1;
                    }
                } else {
                    type = 0;
                    counter = 0;
                }

                if(counter == 3) {
                    return type;
                }
            }

            counter = 0;
            type = 0;
        }

        counter = 0;
        for(int i = 0; i < 16; i++) {
            if(fields[i] == 0) {
                counter++;
            }
        }

        if(counter == 0) {
            return -1;
        }
        return 0;
    }

    private boolean won(int hasWon) {
        if(hasWon != 0) {
            if(hasWon == 1) {
                playerScoreCount++;
                winnerText.setText("Wygrywa gracz");
            } else if(hasWon == 2) {
                computerScoreCount++;
                winnerText.setText("Wygrywa AI");
            } else if(hasWon == -1) {
                winnerText.setText("Remis");
            }

            disableButtons(true);
            playerScore.setText(String.valueOf(playerScoreCount));
            computerScore.setText(String.valueOf(computerScoreCount));
            return true;
        }

        return false;
    }

    private void disableButtons(boolean disable) {
        for(Button btn : buttons) {
            btn.setDisable(disable);
        }
    }

    private void newGame() {
        disableButtons(false);
        winnerText.setText("");

        for(int i = 0; i < 16; i++) {
            fields[i] = 0;
        }

        move = 0;

        for(Button btn : buttons) {
            btn.setText("");
        }
    }

    private static String getFileExtension(File file) {
        String extension = "";

        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                extension = name.substring(name.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }

        return extension;
    }

    public void initialize() {
        buttons.add(btn1);
        buttons.add(btn2);
        buttons.add(btn3);
        buttons.add(btn4);
        buttons.add(btn5);
        buttons.add(btn6);
        buttons.add(btn7);
        buttons.add(btn8);
        buttons.add(btn9);
        buttons.add(btn10);
        buttons.add(btn11);
        buttons.add(btn12);
        buttons.add(btn13);
        buttons.add(btn14);
        buttons.add(btn15);
        buttons.add(btn16);
    }
}
