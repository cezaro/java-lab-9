var getMove = function(fields) {
    var best = {row: -1, col: -1}

    newFields = [];
    for(var i = 0; i < 4; i++) {
        newFields[i] = []

        for(var j = 0; j < 4; j++) {
            newFields[i][j] = fields[i * 4 + j];
        }
    }

    return getIndex(findBestMove(newFields));
}

function getIndex(object) {
    return object.row * 4 + object.col
}

function isMovesLeft(fields) {
    for (var i = 0; i < 4; i++)
        for (var j = 0; j < 4; j++)
            if (fields[i][j] == 0)
                return true;

    return false;
}

function evaluate(fields) {
    for (var i = 0; i < 4; i++) {
        var k = 1;

        if (fields[i][k] != 0) {
            if (fields[i][k - 1] == fields[i][k] && fields[i][k + 1] == fields[i][k]) {
                if (fields[i][k] == 2) {
                    return +100;
                } else if (fields[i][k] == 1) {
                    return -100;
                }
            }

            k++;

            if (fields[i][k] != 0) {
                if (fields[i][k - 1] == fields[i][k] && fields[i][k + 1] == fields[i][k]) {
                    if (fields[i][k] == 2) {
                        return +100
                    } else if (fields[i][k] == 1) {
                        return -100
                    }
                }
            }
        }
    }

    for (var k = 0; k < 4; k++) {
        var i = 1;

        if (fields[i][k] != 0) {
            if (fields[i + 1][k] == fields[i][k] && fields[i - 1][k] == fields[i][k]) {
                if (fields[i][k] == 2) {
                    return +100
                }else if (fields[i][k] == 1) {
                    return -100
                }
            }
        }

        i++;

        if (fields[i][k] != 0) {
            if (fields[i + 1][k] == fields[i][k] && fields[i - 1][k] == fields[i][k]) {
                if (fields[i][k] == 2) {
                    return +100
                } else if (fields[i][k] == 1) {
                    return -100
                }
            }
        }
    }

    for (var i = 1; i < 3; i++) {
        for (var k = 1; k < 3; k++) {
            if (fields[i][k] != 0) {
                if (fields[i - 1][k - 1] == fields[i][k] && fields[i + 1][k + 1] == fields[i][k]) {
                    if (fields[i][k] == 2) {
                        return +100
                    }else if (fields[i][k] == 1) {
                        return -100
                    }
                }

                if (fields[i - 1][k + 1] == fields[i][k] && fields[i + 1][k - 1] == fields[i][k]) {
                    if (fields[i][k] == 2) {
                        return +100
                    } else if (fields[i][k] == 1) {
                        return -100
                    }
                }
            }
        }
    }

    return 0;
}

function minimax(fields, depth, isMax) {
    var score = evaluate(fields);

    if (score > 0)
        return score;

    if (score < 0)
        return score;

    if (isMovesLeft(fields) == false)
        return 0;

    if(depth > 4)
        return score;

    if (isMax) {
        var best = -1000;

        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                if (fields[i][j] == 0) {
                    fields[i][j] = 2;

                    best = Math.max(best, minimax(fields, depth + 1, !isMax));
                    best = best - depth;

                    fields[i][j] = 0;
                }
            }
        }
        return best;
    } else {
        var best = 1000;

        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 4; j++) {
                if (fields[i][j] == 0) {
                    fields[i][j] = 1;

                    best = Math.min(best,minimax(fields, depth + 1, !isMax));
                    best = best + depth;
                    fields[i][j] = 0;
                }
            }
        }

        return best;
    }
}

function findBestMove(fields) {
    var bestVal = -1000;
    var bestMove = {row: -1, col: -1};

    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 4; j++) {
            if (fields[i][j] == 0) {
                fields[i][j] = 2;

                var moveVal = minimax(fields, 0, false);

                fields[i][j] = 0;

                if (moveVal > bestVal) {
                    bestMove = {row: i, col: j};
                    bestVal = moveVal;
                }
            }
        }
    }

    return bestMove;
}