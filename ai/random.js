var getMove = function(fields, player, onlyIndex) {
    var emptyFields = []

    for(var i = 0; i < fields.length; i++) {
        if(fields[i] == 0) {
            emptyFields.push(i)
        }
    }

    if(emptyFields.length == 0) {
        return -1;
    }

    var index = Math.floor(Math.random() * emptyFields.length)
    return emptyFields[index]
};